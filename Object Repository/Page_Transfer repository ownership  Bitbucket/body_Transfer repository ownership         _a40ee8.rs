<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_Transfer repository ownership         _a40ee8</name>
   <tag></tag>
   <elementGuidId>8a515839-00b8-4781-9413-3649cee0fe83</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body.production.adg3.aui-8.aui-legacy-focus.workflow-layout</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>production adg3 aui-8 aui-legacy-focus workflow-layout</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-static-url</name>
      <type>Main</type>
      <value>https://d301sr5gafysq2.cloudfront.net/4ed00b35500a/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-base-url</name>
      <type>Main</type>
      <value>https://bitbucket.org</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-no-avatar-image</name>
      <type>Main</type>
      <value>img/default_avatar/user_blue.svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-current-user</name>
      <type>Main</type>
      <value>{&quot;displayName&quot;: &quot;Kreeti Singh&quot;, &quot;uuid&quot;: &quot;{04aefcb8-abec-4b15-9839-82eacc4d9bdb}&quot;, &quot;hasPremium&quot;: false, &quot;avatarUrl&quot;: &quot;https://secure.gravatar.com/avatar/aab96f67129ded320844a97b64d82d8c?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKS-6.png&quot;, &quot;isTeam&quot;: false, &quot;isSshEnabled&quot;: true, &quot;mention_id&quot;: &quot;5f4fd2f2d0884f0049388e4c&quot;, &quot;isKbdShortcutsEnabled&quot;: true, &quot;avatarUrl2x&quot;: &quot;https://secure.gravatar.com/avatar/aab96f67129ded320844a97b64d82d8c?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKS-6.png&amp;s=64&quot;, &quot;nickname&quot;: &quot;Kreeti Singh&quot;, &quot;id&quot;: 17595812, &quot;isAuthenticated&quot;: true}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-atlassian-id</name>
      <type>Main</type>
      <value>{&quot;loginUrl&quot;: &quot;https://id.atlassian.com/login?continue=https%3A%2F%2Fbitbucket.org%2Ftest-2604%2Ftest-000%2Fadmin%2Ftransfer%2F165447d9ff03a553b1ba64a3aa508c06&amp;prompt=login&quot;, &quot;loginStatusUrl&quot;: &quot;https://id.atlassian.com/profile/rest/profile&quot;}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-settings</name>
      <type>Main</type>
      <value>{&quot;MENTIONS_MIN_QUERY_LENGTH&quot;: 1}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-browser-monitoring</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-switch-create-pullrequest-commit-status</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-atlassian-editor-enabled</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-atlassian-emoji-base-url</name>
      <type>Main</type>
      <value>https://api-private.atlassian.com/emoji/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-aui-version</name>
      <type>Main</type>
      <value>8.7.1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  
  
    
    
    
      
  





      
  

  



      
        

        

        
        
  
    
      
        
          Transfer repository ownership
        
      
    
    
      
        
          
          
          
            
              Priya Mahto
              proposes to transfer ownership of the repository
              test-000.
            
          
        
        
          
        
        
          
          
          
            
              current workspace
              
                
                test-2604
              
            
          
          
            
          
          
            
              new workspace
              
                
                Kreetisingh
              
            
          
        
        
          
          
          
            
              Select the project to which you would like to move this repository:
            
          
          
               Select project   
          
          

          
            
              
                If you accept, all existing users and groups will have their access to the repository revoked.
              
            
          
          
            
          

          
            
              Accept
            
            
              Reject
            
          
        

      
    
  
  
  
  &lt;span class=&quot;project-avatar aui-avatar aui-avatar-squared aui-avatar-project aui-avatar-[[size]] [[extra_classes]]&quot;>
  &lt;span class=&quot;aui-avatar-inner&quot;>
    &lt;img src=&quot;[[#avatar_deferred]][[default_url]][[/avatar_deferred]][[^avatar_deferred]][[avatar_url_2x]][[/avatar_deferred]]&quot; class=&quot;project-avatar-img[[#avatar_deferred]] deferred-image[[/avatar_deferred]]&quot;
        data-default-url=&quot;[[default_url]]&quot;
        [[#avatar_deferred]]
          data-src-url=&quot;[[avatar_url]]&quot; data-src-url-2x=&quot;[[avatar_url_2x]]&quot;
        [[/avatar_deferred]]
        >
  &lt;/span>
&lt;/span>



      
    
    
    
    





  


  
    
    
  
  
    
    
  
  
    
    
  


  
    
    
  



  
    
    
  


    
    
  


  


  
    
  
  
    
    
  
  
    
    
  
  
    
    
  
  
    
    
  




  

  
    
      
        
[[! Escaping can be disabled (with &quot;&amp;&quot;) for these template vars because they're manually escaped in the various consumers ]]
&lt;span class=&quot;mention-result&quot;>
  &lt;span class=&quot;aui-avatar aui-avatar-small mention-result--avatar&quot;>
    &lt;span class=&quot;aui-avatar-inner&quot;>
      &lt;img src=&quot;[[avatar_url]]&quot;>
    &lt;/span>
  &lt;/span>
  [[#display_name]]
    [[#has_distinct_display_name]]
      [[#nickname]]
        &lt;span class=&quot;display-name mention-result--display-name&quot;>[[&amp;display_name]]&lt;/span> &lt;span class=&quot;username mention-result--secondary-name mention-result--username&quot;>([[&amp;nickname]])&lt;/span>
      [[/nickname]]
      [[^nickname]]
        &lt;span class=&quot;display-name mention-result--display-name&quot;>[[&amp;display_name]]&lt;/span> &lt;span class=&quot;username mention-result--secondary-name mention-result--username&quot;>([[&amp;mention_id]])&lt;/span>
      [[/nickname]]
    [[/has_distinct_display_name]]
    [[^has_distinct_display_name]]
      &lt;span class=&quot;display-name mention-result--display-name distinct&quot;>[[&amp;display_name]]&lt;/span>
    [[/has_distinct_display_name]]
  [[/display_name]]
  [[^display_name]]
    [[#nickname]]
      &lt;span class=&quot;username mention-result--username&quot;>[[&amp;nickname]]&lt;/span>
    [[/nickname]]
    [[^nickname]]
      &lt;span class=&quot;username mention-result--username&quot;>[[&amp;mention_id]]&lt;/span>
    [[/nickname]]
  [[/display_name]]
  [[#should_show_review_count]][[#open_review_count]][[^is_team]]
    &lt;span class=&quot;aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge&quot;>
      [[#is_review_count_plural]]
        
          [[open_review_count]] reviews
        
      [[/is_review_count_plural]]
      [[^is_review_count_plural]]
        
          [[open_review_count]] review
        
      [[/is_review_count_plural]]
    &lt;/span>
  [[/is_team]][[/open_review_count]][[/should_show_review_count]]
  [[#is_teammate]][[^is_team]]
    &lt;span class=&quot;aui-lozenge aui-lozenge-complete aui-lozenge-subtle mention-result--lozenge&quot;>teammate&lt;/span>
  [[/is_team]][[/is_teammate]]
&lt;/span>

      
    
      
        
[[^query]]
&lt;li class=&quot;bb-typeahead-item&quot;>Begin typing to search for a user&lt;/li>
[[/query]]
[[#query]]
&lt;li class=&quot;bb-typeahead-item&quot;>Continue typing to search for a user&lt;/li>
[[/query]]

      
    
      
        
[[^searching]]
&lt;li class=&quot;bb-typeahead-item&quot;>Found no matching users for &lt;em>[[query]]&lt;/em>.&lt;/li>
[[/searching]]
[[#searching]]
&lt;li class=&quot;bb-typeahead-item bb-typeahead-searching&quot;>Searching for &lt;em>[[query]]&lt;/em>.&lt;/li>
[[/searching]]

      
    
      
        
&lt;span class=&quot;emoji-result&quot;>
  &lt;span class=&quot;emoji-result--avatar&quot;>
    &lt;img class=&quot;emoji&quot; src=&quot;[[src]]&quot;>
  &lt;/span>
  &lt;span class=&quot;name emoji-result--name&quot;>[[&amp;name]]&lt;/span>
&lt;/span>

      
    
      
        &lt;span class=&quot;aui-avatar aui-avatar-project aui-avatar-xsmall&quot;>
  &lt;span class=&quot;aui-avatar-inner&quot;>
    &lt;img src=&quot;[[avatar]]&quot;>
  &lt;/span>
&lt;/span>
&lt;span class=&quot;owner&quot;>[[&amp;owner]]&lt;/span>/&lt;span class=&quot;slug&quot;>[[&amp;slug]]&lt;/span>

      
    
      
        &lt;ul class=&quot;scope-list&quot;>
  [[#scopes]]
    &lt;li class=&quot;scope-list--item&quot;>[[description]]&lt;/li>
  [[/scopes]]
&lt;/ul>

      
    
      
        &lt;div class=&quot;omnibar-input-container&quot;>
  &lt;input class=&quot;omnibar-input&quot; type=&quot;text&quot; [[#placeholder]]placeholder=&quot;[[placeholder]]&quot;[[/placeholder]]>
&lt;/div>
&lt;ul class=&quot;omnibar-result-group-list&quot;>&lt;/ul>

      
    
      
        

&lt;div class=&quot;omnibar-blank-slate&quot;>No results found&lt;/div>

      
    
      
        &lt;div class=&quot;omnibar-result-group-header clearfix&quot;>
  &lt;h2 class=&quot;omnibar-result-group-label&quot; title=&quot;[[label]]&quot;>[[label]]&lt;/h2>
  &lt;span class=&quot;omnibar-result-group-context&quot; title=&quot;[[context]]&quot;>[[context]]&lt;/span>
&lt;/div>
&lt;ul class=&quot;omnibar-result-list unstyled-list&quot;>&lt;/ul>

      
    
      
        [[#url]]
  &lt;a href=&quot;[[&amp;url]]&quot; class=&quot;omnibar-result-label&quot;>[[&amp;label]]&lt;/a>
[[/url]]
[[^url]]
  &lt;span class=&quot;omnibar-result-label&quot;>[[&amp;label]]&lt;/span>
[[/url]]
[[#context]]
  &lt;span class=&quot;omnibar-result-context&quot;>[[context]]&lt;/span>
[[/context]]

      
    
  




  
  



  window.__initial_state__ = {&quot;global&quot;: {&quot;theme&quot;: null, &quot;isPullRequestSingleFileModeEnabled&quot;: false, &quot;features&quot;: {&quot;perms-facade-apis-adminhub&quot;: true, &quot;add-metrics-for-permissions-queries&quot;: true, &quot;new-code-review-onboarding-experience&quot;: true, &quot;account-switcher&quot;: true, &quot;sync-aid-revoked-to-workspace&quot;: true, &quot;fabric-user-picker&quot;: true, &quot;show-guidance-message&quot;: true, &quot;fd-prs-client-cache-fallback&quot;: true, &quot;markdown-embedded-html&quot;: false, &quot;prlinks-installer&quot;: true, &quot;fetch-all-relevant-jira-projects&quot;: true, &quot;nav-next-settings&quot;: true, &quot;jira-tab-searchable-assignees-filter&quot;: true, &quot;show-pr-update-activity-changes&quot;: true, &quot;clone-in-xcode&quot;: true, &quot;new-code-review&quot;: true, &quot;atlassian-editor&quot;: true, &quot;tns-to-stripe-migration&quot;: true, &quot;resolve-uuids-in-find-raw-choice&quot;: true, &quot;read-only-message-migrations&quot;: true, &quot;repo-show-uuid&quot;: false, &quot;support-sending-custom-events-to-the-webhook-processor&quot;: true, &quot;spa-repo-settings--access-keys&quot;: true, &quot;whitelisted_throttle_exemption&quot;: true, &quot;orochi-git-diff-refactor&quot;: true, &quot;uninstall-dvcs-addon-only-when-jira-is-removed&quot;: true, &quot;custom-default-branch-name-repo-create&quot;: true, &quot;restrict-commit-author-data&quot;: true, &quot;use-users-with-privilege&quot;: true, &quot;add-member-to-workspace-apis&quot;: true, &quot;rm-empty-ref-dirs-on-push&quot;: true, &quot;provision-workspaces-in-hams&quot;: true, &quot;record-site-addon-version&quot;: true, &quot;block-oauth-connect-callback&quot;: true, &quot;show-banner-about-new-review-experience&quot;: true, &quot;reviewer-status&quot;: true, &quot;use-py-hams-client-asap&quot;: true, &quot;provisioning-install-pipelines-addon&quot;: true, &quot;disable-hg&quot;: true, &quot;nav-add-file&quot;: false, &quot;enable-merge-bases-api&quot;: true, &quot;parcel-frontbucket-bundles&quot;: true, &quot;log-asap-errors&quot;: true, &quot;exp-new-user-survey&quot;: true, &quot;lookup-pr-approvers-from-prs&quot;: true, &quot;free-daily-repo-limit&quot;: true, &quot;allocate-with-regions&quot;: true, &quot;exp-share-to-invite-variation&quot;: true, &quot;fd-ie-deprecation-phase-one&quot;: true, &quot;remove-deactivated-users-from-followers&quot;: true, &quot;allow-users-members-endpoint&quot;: true, &quot;expand-accesscontrol-cache-key&quot;: true, &quot;api-diff-caching&quot;: true, &quot;backbone-radio&quot;: true, &quot;for-member-query-metrics&quot;: true, &quot;fd-jira-migration&quot;: true, &quot;clonebundles&quot;: true, &quot;fd-add-gitignore-dropdown-on-create-repo-page&quot;: true, &quot;fd-ie-deprecation-phase-two&quot;: true, &quot;lsn-based-db-routing&quot;: true, &quot;svg-based-qr-code&quot;: true, &quot;provisioning-auto-login&quot;: true, &quot;frontbucket-leave-repository&quot;: true, &quot;connect-iframe-sandbox&quot;: false, &quot;view-source-filtering-upon-timeout&quot;: true, &quot;fd-overview-page-pr-filter-buttons&quot;: true, &quot;new-analytics-cdn&quot;: true, &quot;facade-apis-metrics&quot;: true, &quot;frontbucket-eager-dispatching-of-exited-code-review&quot;: true, &quot;show-upgrade-plans-banner&quot;: true, &quot;jquery3&quot;: true, &quot;break-login-loop&quot;: true, &quot;use-elasticache-lsn-storage&quot;: true, &quot;workspaces-api-proxy&quot;: true, &quot;webhook_encryption_disabled&quot;: true, &quot;connect-iframe-no-sub&quot;: true, &quot;orochi-large-merge-message-support&quot;: true, &quot;large-pr-rendering-limits&quot;: true, &quot;enable-fx3-client&quot;: true, &quot;fd-jira-compatible-issue-export&quot;: true, &quot;block-hg-repo-forks&quot;: true, &quot;provisioning-skip-workspace-creation&quot;: true, &quot;enable-jwt-repo-filtering&quot;: true, &quot;ssr-default&quot;: true, &quot;do-not-suppress-exception-ws-syncer&quot;: true, &quot;use-moneybucket&quot;: true, &quot;pride-logo&quot;: false, &quot;pr_post_build_merge&quot;: false, &quot;bms-repository-no-finalize&quot;: true, &quot;orochi-disable-hooks-with-lockid&quot;: true, &quot;consenthub-config-endpoint-update&quot;: true, &quot;fd-undo-last-push&quot;: false, &quot;bbcdev-13546-caching-defaults&quot;: true, &quot;spa-repo-settings--repo-details&quot;: true, &quot;lsn-lookups&quot;: true, &quot;orochi-custom-default-branch-name-repo-create&quot;: true, &quot;auth-flow-adg3&quot;: true, &quot;disable-repository-replication-task&quot;: true, &quot;workspace-ui&quot;: true, &quot;hot-91446-add-tracing-x-b3&quot;: true, &quot;hide-price-annual&quot;: true, &quot;allow-cloud-session&quot;: true}, &quot;locale&quot;: &quot;en&quot;, &quot;isCodeReviewWelcomeDialogOpen&quot;: false, &quot;is_mobile_user_agent&quot;: false, &quot;site_message&quot;: &quot;&quot;, &quot;codeReviewSidebarWidth&quot;: &quot;280&quot;, &quot;isPullRequestIgnoreWhitespaceEnabled&quot;: false, &quot;sourceBrowserSidebarWidth&quot;: &quot;280&quot;, &quot;geoip_country&quot;: &quot;IN&quot;, &quot;isFocusedTask&quot;: true, &quot;isCodeReviewSidebarOpen&quot;: true, &quot;focusedTaskBackButtonUrl&quot;: null, &quot;importBitbucketActions&quot;: [{&quot;analytics_label&quot;: null, &quot;is_client_link&quot;: false, &quot;icon_class&quot;: &quot;&quot;, &quot;badge_label&quot;: null, &quot;weight&quot;: 100, &quot;url&quot;: &quot;/repo/import&quot;, &quot;tab_name&quot;: null, &quot;can_display&quot;: true, &quot;children&quot;: [], &quot;type&quot;: &quot;menu_item&quot;, &quot;anchor&quot;: true, &quot;analytics_payload&quot;: {}, &quot;matching_url_prefixes&quot;: [], &quot;label&quot;: &quot;&lt;strong>Repository&lt;\/strong>&quot;, &quot;target&quot;: &quot;_self&quot;, &quot;id&quot;: &quot;repository-import-drawer-item&quot;, &quot;icon&quot;: &quot;&quot;}], &quot;isPullRequestAnnotationsEnabled&quot;: true, &quot;browser_monitoring&quot;: true, &quot;bitbucketActions&quot;: [{&quot;analytics_label&quot;: null, &quot;is_client_link&quot;: false, &quot;icon_class&quot;: &quot;&quot;, &quot;badge_label&quot;: null, &quot;weight&quot;: 100, &quot;url&quot;: &quot;/repo/create&quot;, &quot;tab_name&quot;: null, &quot;can_display&quot;: true, &quot;children&quot;: [], &quot;type&quot;: &quot;menu_item&quot;, &quot;anchor&quot;: true, &quot;analytics_payload&quot;: {}, &quot;matching_url_prefixes&quot;: [], &quot;label&quot;: &quot;&lt;strong>Repository&lt;\/strong>&quot;, &quot;target&quot;: &quot;_self&quot;, &quot;id&quot;: &quot;repository-create-drawer-item&quot;, &quot;icon&quot;: &quot;&quot;}, {&quot;analytics_label&quot;: null, &quot;is_client_link&quot;: false, &quot;icon_class&quot;: &quot;&quot;, &quot;badge_label&quot;: null, &quot;weight&quot;: 110, &quot;url&quot;: &quot;/workspace/create/&quot;, &quot;tab_name&quot;: null, &quot;can_display&quot;: true, &quot;children&quot;: [], &quot;type&quot;: &quot;menu_item&quot;, &quot;anchor&quot;: true, &quot;analytics_payload&quot;: {}, &quot;matching_url_prefixes&quot;: [], &quot;label&quot;: &quot;&lt;strong>Workspace&lt;\/strong>&quot;, &quot;target&quot;: &quot;_self&quot;, &quot;id&quot;: &quot;workspace-create-drawer-item&quot;, &quot;icon&quot;: &quot;&quot;}, {&quot;analytics_label&quot;: null, &quot;is_client_link&quot;: false, &quot;icon_class&quot;: &quot;&quot;, &quot;badge_label&quot;: null, &quot;weight&quot;: 120, &quot;url&quot;: &quot;/account/projects/create&quot;, &quot;tab_name&quot;: null, &quot;can_display&quot;: true, &quot;children&quot;: [], &quot;type&quot;: &quot;menu_item&quot;, &quot;anchor&quot;: true, &quot;analytics_payload&quot;: {}, &quot;matching_url_prefixes&quot;: [], &quot;label&quot;: &quot;&lt;strong>Project&lt;\/strong>&quot;, &quot;target&quot;: &quot;_self&quot;, &quot;id&quot;: &quot;project-create-drawer-item&quot;, &quot;icon&quot;: &quot;&quot;}, {&quot;analytics_label&quot;: null, &quot;is_client_link&quot;: false, &quot;icon_class&quot;: &quot;&quot;, &quot;badge_label&quot;: null, &quot;weight&quot;: 130, &quot;url&quot;: &quot;/snippets/new&quot;, &quot;tab_name&quot;: null, &quot;can_display&quot;: true, &quot;children&quot;: [], &quot;type&quot;: &quot;menu_item&quot;, &quot;anchor&quot;: true, &quot;analytics_payload&quot;: {}, &quot;matching_url_prefixes&quot;: [], &quot;label&quot;: &quot;&lt;strong>Snippet&lt;\/strong>&quot;, &quot;target&quot;: &quot;_self&quot;, &quot;id&quot;: &quot;snippet-create-drawer-item&quot;, &quot;icon&quot;: &quot;&quot;}], &quot;isPullRequestColorBlindModeEnabled&quot;: false, &quot;commitViewSidebarWidth&quot;: null, &quot;pullRequestFileViewMode&quot;: null, &quot;path&quot;: &quot;/test-2604/test-000/admin/transfer/165447d9ff03a553b1ba64a3aa508c06&quot;, &quot;currentUser&quot;: {&quot;has_2fa_enabled&quot;: true, &quot;display_name&quot;: &quot;Kreeti Singh&quot;, &quot;uuid&quot;: &quot;{04aefcb8-abec-4b15-9839-82eacc4d9bdb}&quot;, &quot;links&quot;: {&quot;self&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/!api/2.0/users/%7B04aefcb8-abec-4b15-9839-82eacc4d9bdb%7D&quot;}, &quot;html&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/%7B04aefcb8-abec-4b15-9839-82eacc4d9bdb%7D/&quot;}, &quot;avatar&quot;: {&quot;href&quot;: &quot;https://secure.gravatar.com/avatar/aab96f67129ded320844a97b64d82d8c?d=https%3A%2F%2Favatar-management--avatars.us-west-2.prod.public.atl-paas.net%2Finitials%2FKS-6.png&quot;}}, &quot;extra&quot;: {&quot;has_ssh_key&quot;: true, &quot;has_premium&quot;: false, &quot;has_atlassian_account&quot;: true, &quot;workspace_id&quot;: &quot;Kreetisingh&quot;, &quot;team_workspace_name&quot;: &quot;juspay-transfer&quot;}, &quot;type&quot;: &quot;user&quot;, &quot;zoneinfo&quot;: null, &quot;account_status&quot;: &quot;active&quot;, &quot;created_on&quot;: &quot;2020-09-02T17:14:55.976079+00:00&quot;, &quot;is_staff&quot;: false, &quot;location&quot;: null, &quot;department&quot;: null, &quot;organization&quot;: null, &quot;job_title&quot;: &quot;QA Engineer&quot;, &quot;nickname&quot;: &quot;Kreeti Singh&quot;, &quot;properties&quot;: {}, &quot;account_id&quot;: &quot;5f4fd2f2d0884f0049388e4c&quot;}, &quot;isPullRequestWordDiffEnabled&quot;: true, &quot;needs_marketing_consent&quot;: false, &quot;isCommitViewSidebarOpen&quot;: true, &quot;pullRequestDiffViewMode&quot;: null, &quot;teams&quot;: [{&quot;display_name&quot;: &quot;Juspay&quot;, &quot;name&quot;: &quot;Juspay&quot;, &quot;links&quot;: {&quot;self&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/!api/2.0/workspaces/juspay&quot;}, &quot;html&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/juspay/&quot;}, &quot;avatar&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/workspaces/juspay/avatar/?ts=1611455089&quot;}}, &quot;type&quot;: &quot;workspace&quot;, &quot;slug&quot;: &quot;juspay&quot;, &quot;uuid&quot;: &quot;{fbde899f-536a-48a7-b69c-5d3397621e67}&quot;}, {&quot;display_name&quot;: &quot;juspay-transfer&quot;, &quot;name&quot;: &quot;juspay-transfer&quot;, &quot;links&quot;: {&quot;self&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/!api/2.0/workspaces/juspay-transfer&quot;}, &quot;html&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/juspay-transfer/&quot;}, &quot;avatar&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/workspaces/juspay-transfer/avatar/?ts=1612778017&quot;}}, &quot;type&quot;: &quot;workspace&quot;, &quot;slug&quot;: &quot;juspay-transfer&quot;, &quot;uuid&quot;: &quot;{4e4abdfc-32d5-4d31-a802-73560a921acf}&quot;}, {&quot;display_name&quot;: &quot;Kreeti Singh&quot;, &quot;name&quot;: &quot;Kreeti Singh&quot;, &quot;links&quot;: {&quot;self&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/!api/2.0/workspaces/Kreetisingh&quot;}, &quot;html&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/Kreetisingh/&quot;}, &quot;avatar&quot;: {&quot;href&quot;: &quot;https://bitbucket.org/workspaces/Kreetisingh/avatar/?ts=1599066896&quot;}}, &quot;type&quot;: &quot;workspace&quot;, &quot;slug&quot;: &quot;Kreetisingh&quot;, &quot;uuid&quot;: &quot;{04aefcb8-abec-4b15-9839-82eacc4d9bdb}&quot;}], &quot;pullRequestDiffTabSize&quot;: null, &quot;flags&quot;: [], &quot;isNavigationOpen&quot;: true, &quot;isSourceBrowserSidebarOpen&quot;: true, &quot;whats_new_feed&quot;: &quot;https://bitbucket.org/blog/wp-json/wp/v2/posts?categories=196&amp;context=embed&amp;per_page=6&amp;orderby=date&amp;order=desc&quot;}};
  window.__settings__ = {&quot;MARKETPLACE_TERMS_OF_USE_URL&quot;: null, &quot;JIRA_ISSUE_COLLECTORS&quot;: {&quot;code-review-beta&quot;: {&quot;url&quot;: &quot;https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-4bqv2z/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&amp;collectorId=bb066400&quot;, &quot;id&quot;: &quot;bb066400&quot;}, &quot;jira-software-repo-page&quot;: {&quot;url&quot;: &quot;https://jira.atlassian.com/s/1ce410db1c7e1b043ed91ab8e28352e2-T/yl6d1c/804001/619f60e5de428c2ed7545f16096c303d/3.1.0/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-UK&amp;collectorId=064d6699&quot;, &quot;id&quot;: &quot;064d6699&quot;}, &quot;code-review-rollout&quot;: {&quot;url&quot;: &quot;https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-4bqv2z/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&amp;collectorId=de003e2d&quot;, &quot;id&quot;: &quot;de003e2d&quot;}, &quot;source-browser&quot;: {&quot;url&quot;: &quot;https://bitbucketfeedback.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-tqnsjm/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&amp;collectorId=c19c2ff6&quot;, &quot;id&quot;: &quot;c19c2ff6&quot;}}, &quot;STATUSPAGE_URL&quot;: &quot;https://bitbucket.status.atlassian.com/&quot;, &quot;CANON_URL&quot;: &quot;https://bitbucket.org&quot;, &quot;CONSENT_HUB_FRONTEND_BASE_URL&quot;: &quot;https://preferences.atlassian.com&quot;, &quot;API_CANON_URL&quot;: &quot;https://api.bitbucket.org&quot;, &quot;SOCIAL_AUTH_ATLASSIANID_LOGOUT_URL&quot;: &quot;https://id.atlassian.com/logout&quot;, &quot;EMOJI_STANDARD_BASE_URL&quot;: &quot;https://api-private.atlassian.com/emoji/&quot;};
  window.__webpack_nonce__ = 'fBER9wydlk2KXIDN';




  


  


Flag notifications




window.NREUM||(NREUM={});NREUM.info={&quot;beacon&quot;:&quot;bam-cell.nr-data.net&quot;,&quot;queueTime&quot;:0,&quot;licenseKey&quot;:&quot;a2cef8c3d3&quot;,&quot;agent&quot;:&quot;&quot;,&quot;transactionName&quot;:&quot;NFcGYEdUW0IAVE1QCw0dIkFbVkFYDlkWWw0XUBFXXlBBHwBHSUpKAVBKQlxQQkJbVl1UDQ1tHFJQRw==&quot;,&quot;applicationID&quot;:&quot;548124220,1841284&quot;,&quot;errorBeacon&quot;:&quot;bam-cell.nr-data.net&quot;,&quot;applicationTime&quot;:256}


id(&quot;select2-drop-mask&quot;)                Searching...</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;windows chrome&quot;]/body[@class=&quot;production adg3 aui-8 aui-legacy-focus workflow-layout&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body</value>
   </webElementXpaths>
</WebElementEntity>
